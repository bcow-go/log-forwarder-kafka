module gitlab.bcowtech.de/bcow-go/log-forwarder-kafka

go 1.14

require (
	github.com/confluentinc/confluent-kafka-go v1.4.2 // indirect
	gitlab.bcowtech.de/bcow-go/log v1.3.0
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.4.2
)
